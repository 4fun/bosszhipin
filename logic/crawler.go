package logic

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"regexp"
	"strings"

	"gitea.com/4fun/bosszhipin/util"

	"gitea.com/4fun/bosszhipin/model"
	"github.com/gocolly/colly"
)

func GetJobinfo() {
	baseUrl := "https://www.zhipin.com"
	urlSuffix := "/job_detail/?query=%E4%BC%9A%E8%AE%A1&city=101120600&industry=&position="
	jobInfos := make([]model.JobInfo, 0, 200)
	jobDetails := make([]model.JobDetail, 0, 200)
	c := colly.NewCollector(
		colly.UserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36"),
	)
	// 复制collector作为详情爬虫器
	d := c.Clone()
	//爬取工作基本信息
	c.OnHTML("div.job-list>ul", func(e *colly.HTMLElement) {
		e.ForEach("li", func(i int, el *colly.HTMLElement) {
			j := model.JobInfo{}
			// 工作名称 薪水
			j.JobName = el.ChildText("div.job-title")
			j.Salary = el.ChildText("span.red")
			// 定义函数
			getInfo := func(goquerySelector string, reg string) []string {
				r := regexp.MustCompile(reg)
				info, err := el.DOM.Find(goquerySelector).Html()
				if err != nil {
					log.Fatal(err)
				}
				s := r.Split(info, -1)
				return s
			}
			// 地址 工作年限和教育程度
			s := getInfo("div.info-primary>p", `<.*?></.*?>`)
			j.Address = strings.Join(s[0:len(s)-2], "")
			j.Experience = s[len(s)-2]
			j.Education = s[len(s)-1]
			// 公司名称
			j.Company = el.ChildText("div.info-company h3 a")
			s = getInfo("div.company-text>p", `<.*?></.*?>`)
			j.Employees = s[len(s)-1]
			// 工作详情URL
			urlTmp := el.ChildAttr("a[data-jid]", "href")
			j.URL = baseUrl + util.SubstringWithoutBegin(urlTmp, strings.Index(urlTmp, "."))
			jobInfos = append(jobInfos, j)
			d.Visit(j.URL)
		})
	})
	c.OnScraped(func(_ *colly.Response) {
		data, err := json.MarshalIndent(jobInfos, "", "\t")
		if err != nil {
			log.Fatal(err)
		}
		ioutil.WriteFile("jobs.txt", data, 0666)
	})
	d.OnHTML("div.job-detail", func(e *colly.HTMLElement) {
		j := model.JobDetail{}
		j.JobDescription = e.ChildText("div.text")
		info, err := e.DOM.Find("div.job-tags").Html()
		if err != nil {
			log.Fatal(err)
		}
		j.JobTeam = info //TODO need to be handled
		j.ComIntroduce = e.ChildText("div.company-info:nth-child(2)")
		j.ComAddress = e.ChildText("div.location-address")
		jobDetails = append(jobDetails, j)
	})
	d.OnScraped(func(_ *colly.Response) {

	})
	c.Visit(baseUrl + urlSuffix)
}
