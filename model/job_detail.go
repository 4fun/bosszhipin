package model

import "time"

type JobDetail struct {
	JobID          string `gorm:"primary_key"`
	JobDescription string
	JobTeam        string
	ComIntroduce   string
	ComAddress     string
	CreatedAt      time.Time
}
