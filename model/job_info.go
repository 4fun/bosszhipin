package model

import "time"

type JobInfo struct {
	ID         string
	JobName    string
	Salary     string
	Address    string
	Experience string
	Education  string
	Company    string
	Employees  string
	Welfare    string
	URL        string
	CreatedAt  time.Time
}
