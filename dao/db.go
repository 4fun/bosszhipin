package dao

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func init() {
	db, err := gorm.Open("sqlite3", "boss.db")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()
}
