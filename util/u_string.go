package util

func SubstringWithoutEnd(s string, begin int) string {
	return Substring(s, begin, len(s))
}

func SubstringWithoutBegin(s string, end int) string {
	return Substring(s, 0, end)
}

func Substring(s string, begin, end int) string {
	if begin < 0 || end > len(s) || begin > end {
		return ""
	}
	if begin == 0 && end == len(s) {
		return s
	}
	r := []rune(s)
	return string(r[begin:end])
}
